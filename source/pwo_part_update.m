function [ error_message ] = pwo_part_update( input_params )
%PWO_PART_UPDATE  updates the part for the chips defined for a given die to pick list of chips, and a wo).
%   This is the real processing function. Source: Ryans: C:\Users\jsanchez\Documents\dcrc\source\pwo\genalyte_webservice_pick_to_workorder
% pipleine:  pick_to_wo (main) --> pwo_part_update  --> fetchInventoryItems
%
error_message = '';
% debug_flag = true;
% if (debug_flag)
%     input_params
% end
webservice_url              = input_params.webservice_url; % 'http://10.0.2.226/runcard/soap?wsdl';          % for real
%                                                          % 'http://10.0.2.226/runcard_test/soap?wsdl';    % test ONLY
wo_number                   = input_params.wo_number;
part_number                 = input_params.part_number;    %
warehouse_bin               = input_params.warehouse_bin;  %
warehouse_loc               = input_params.warehouse_loc;  %
user_name                   = input_params.user_name;      %
part_revision               = '01'; %jas_temp_hardcoded
createClassFromWsdl(webservice_url);
obj = runcard_wsdl;
%methods(obj)


ok_cnt = 0;
for ndx=1:length(input_params.die_pick_to_wo_data)
    %check that this serial number is valid, need its status as well
    [response, error, msg] = fetchInventoryItems(obj, input_params.die_pick_to_wo_data{ndx},'', '', '', '', '', '', '', '');
    if error ~= 0
        disp(['RunCard exception encountered (fetchInventoryItems): ' msg]);
        return
    end
    
    if isempty(response)
        disp(['Serial ' input_params.die_pick_to_wo_data{ndx} ' does not exist in RunCard database.']);
        continue
    end
    %error =0;
    %msg='';
    try
        [error, msg] = pickUnitToWorkOrder(obj,input_params.die_pick_to_wo_data{ndx}, part_number, part_revision, wo_number, warehouse_bin, warehouse_loc, user_name);
        if error ~= 0
            disp(['RunCard exception for ' input_params.die_pick_to_wo_data{ndx} ': ' msg]);
        else
            disp([input_params.die_pick_to_wo_data{ndx} ' added to ' wo_number]);
            ok_cnt = ok_cnt+1;
        end
    catch
        disp(['RunCard EXCEPTION for ' input_params.die_pick_to_wo_data{ndx} ' ' ]);
        continue;
    end
end

msg = sprintf('Summary: Picked  %-4d / %-4d  To Work Order: %-s', ok_cnt,length(input_params.die_pick_to_wo_data),wo_number);
fprintf('\n%-s\n',msg);
error_message = 'PICK TO WORK ORDER DONE';
end % fn pwo_part_update


% leftovers from the die_scrap:
% % %
% % % %check the status of each serial number
% % % for ndx=1:size(input_params.fail_chip_serials,1)
% % %
% % %     serial = input_params.fail_chip_serials{ndx};   % jas_here_mario_define_sample_input
% % %
% % %     [response, error, msg] = getUnitStatus(obj,serial);
% % %     if error ~= 0
% % %         disp(['RunCard exception encountered : ' msg    ' No die scrapping done at all' ]);
% % %         return
% % %     end
% % % %     if ~strcmp(response.opcode, part_number)
% % % %         disp(['Invalid opcode ' response.opcode ', expecting ' part_number ' skipping serial '  serial ]);
% % % %         continue
% % % %     end
% % %
% % %     if ~strcmp(response.workorder, wo_number)
% % %         error_message = sprintf ('Invalid workorder %-s  expecting %-s skipping serial %-s ', response.workorder ,wo_number, response.serial);
% % %         disp(error_message);
% % %         continue
% % %     end
% % %
% % %     if ~strcmp(response.status, 'IN QUEUE') && ~strcmp(response.status, 'IN PROGRESS')
% % %         error_message = sprintf ('Invalid status %-s  for serial %-s Skipped ', response.status , response.serial  );
% % %         disp(error_message);
% % %         continue
% % %     end
% % %
% % %     disp([response.workorder ': ' response.serial ' is ' response.status]);
% % %
% % %     transactionInfo.username        = user_name;
% % %     transactionInfo.transaction     = trans_type;
% % %     transactionInfo.serial          = response.serial;
% % %     transactionInfo.workorder       = response.workorder;
% % %     transactionInfo.seqnum          = response.seqnum;
% % %     transactionInfo.opcode          = response.opcode;
% % %
% % %     transactionInfo.warehousebin    = warehouse_bin;
% % %     transactionInfo.warehouseloc    = warehouse_loc;
% % %     transactionInfo.comment         = comment;
% % %
% % %     transactionData                 = '';
% % %     transactionBOM                  = '';
% % %
% % %     [error, msg] = transactUnit(obj,transactionInfo, transactionData, transactionBOM);
% % %     if error ~= 0
% % %         disp(['RunCard exception encountered when doing a ' transactionInfo.transaction ' transaction: : ' msg]);
% % %         return
% % %     else
% % %         disp(msg);
% % %     end
% % % end % for each failure chip serial
% % %
% % %




%             ,'part_number',''...
%             ,'warehouse_bin',''...
%             ,'warehouse_loc',''...
%             ,'user_name',''...
%             ,'trans_type',''...
%             ,'comment',''...

