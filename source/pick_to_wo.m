% Main file: pick_to_wo.m
% Script to perform the pick_to_wo given 1 excel file: pick_to_wo_configure, with 2 tabs: 1_ pick_to_wo_configure 2_ pick_to_wo_data
% pick_to_wo_app:
% %       h = matlab.desktop.editor.getAll; h.close
%     cd 'C:\Users\jsanchez\Documents\pick_to_wo\source\'
% to compile:
%   mcc -m  pick_to_wo -a ..\configure  -o  pick_to_wo
%   mcc -m  pick_to_wo  -a ..\configure  -o  pick_to_wo -R 'logfile','pick_to_wo.log'

%   !copy pick_to_wo.exe C:\Users\jsanchez\Documents\pick_to_wo\exe\
%   !move pick_to_wo.exe \\genstore2\Users\Storage\jsanchez\Documents\pick_to_wo\exe\
%   ml_doc: https://www.mathworks.com/products/compiler/supported/compiler_support.html

% Define Arguments: (see end of this file)

% to test:  runcard_test: use wo: DWO-0051 

% pipeline: pick_to_wo --> pwo_get_input_params  --> pwo_get_excel_input_params (excel)  -->  sp_get_excel_range (excel)
%                     --> pwo_part_bin_update  
% Rev_history
% Rev 1.00 Date: 2018_04_24 First compiled version: (clone from the pick_to_wo app) 
% Rev 1.01 Date: 2018_04_12 Re-compiled version: No jas_changes. Maybe Ryan's changes. Just a wid guess. 
run_date = datestr(now);
fprintf('\n Initializing Pick To Work Order:   Rev 1.01  Production \n Run Date-time                      %-s\n',run_date);
[ input_params,error_message ] = pwo_get_input_params();
if (~(isempty(error_message)))
    fprintf('\n ERROR:\n%s\n',error_message);
    fprintf('\n');
    pause(20);
    return;
end
[ error_message ]  = pwo_part_update( input_params);
if ((~(isempty(error_message))) && (~(strcmpi(error_message,'PICK TO WORK ORDER DONE'))))
    fprintf('\nERROR:,\n%s\n',error_message);
end
fprintf('\n\tDIE PICK TO WORK ORDER REQUEST DONE_________________________\n\n'); 
% _______________________________________________________________________________________________________
if (isdeployed)
    fprintf('\n To produce a TEXT report of this output:  \n');    
    fprintf('\n    1. Copy to the clipboard all the output do: Edit_menu --> Select All --> Enter  ');
    fprintf('\n    2. Paste into an email message: CTRL-V to share the report with the Die Scrap Requester. \n');      
    fprintf('\n To verify results: Follow these steps in RunCard:  \n');  
    fprintf('\n   1. Search for the needed WO in Terminal');  
    fprintf('\n   2. In the terminal view select  ... more input from Mario ...');  
    fprintf('\n   3. INPUT FROM MARIO TBD In the View List drop-down select carrier view. ... more input from Mario ...');  
    fprintf('\n   4. INPUT FROM MARIO TBD The colors of each Gelpack in carrier view should reflect the new part_bin_numbers ... more input from Mario ...');
    fprintf('\n\n Bye ... \n');    
    % ALLOW THE USER TO DO CTRL-A CTRL-C
    pause(40); 
end
