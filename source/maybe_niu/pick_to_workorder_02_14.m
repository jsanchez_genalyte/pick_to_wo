% file: 
% Web service example
webservice_url = 'http://10.0.2.226/runcard/soap?wsdl';

createClassFromWsdl(webservice_url);
obj = runcard_wsdl;
%methods(obj)

%serials - cell array of serial numbers (ex. {'P163692.03.001.023', ...}
partNumber = '29005';
partRevision = '01';

targetWorkOrder = 'DWO-0059';
warehouseBin    = 'WIP';                 % 'WIP';              is this where it actually is or where it is going to? It has to be WIP (what is BIN?)
warehouseLoc    = 'Production Floor';    % 'Production Floor'; % 'Consumable Inventory' like a reagent? that it gets swalloed when used? 
operator        = 'jsanchez';

for ndx=1:length(serials)
    
    %check that this serial number is valid, need its status as well
    [response, error, msg] = fetchInventoryItems(obj, serials{ndx},'', '', '', '', '', '', '', '');
    if error ~= 0
        disp(['RunCard exception encountered (fetchInventoryItems): ' msg]);
        return
    end

    if isempty(response)
        disp(['Serial ' serials{ndx} ' does not exist in RunCard database.']);
        continue
    end

    [error, msg] = pickUnitToWorkOrder(obj,serials{ndx}, partNumber, partRevision, targetWorkOrder, warehouseBin, warehouseLoc, operator);
    if error ~= 0
        disp(['RunCard exception for ' serials{ndx} ': ' msg]);
    else
        disp([serials{ndx} ' added to ' targetWorkOrder]);
    end
end

% in run_card/inventory: Filter by:  Part Number:29005 Serial:P153060.0 
% msg: = Unit P153060.6.024.019 is not enabled for consumption.  Please revise your selection.   What to do?
%  status: needs to be available. 
% mgm -> products filter by: Part Number:00026   this is the spotted chip 



