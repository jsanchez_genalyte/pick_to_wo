
% Executable:       = fsr_die_exclusions  
% cfg_file_name     = 'c:\Users\jsanchez\Documents\dcrc\data\fsr_die_exclusions_cfg.txt  % hardcoded file. 
% cfg_file_contents:
%
% function_name     = fsr_die_exclusions
% targetOpCode      = 'Q410';        % ?Optical Test Evaluation? operation code for test step
% warehousebin      = 'SCRAP';       % Bin assignment required in RunCard
% warehouseloc      = 'Scrap Bin';   % Location defined in RunCard to place scrapped chips
% username          = 'mornelas';
% transType         = 'Scrap';
% comment           = 'FSR Failures';
% chips_file_name   = 'c:\Users\jsanchez\Documents\dcrc\data\fsr_die_exclusions_2018_02_06.xlsx' % excel file with chips.

% first tab in excel file will have the list of exlusions. just the chip serials: no quotes, just clean.

% file: genalyte_webservice_FSR_die_scrapping
% Initialize web service
webservice_url = 'http://10.0.2.226/runcard/soap?wsdl';          % for real
%webservice_url = 'http://10.0.2.226/runcard_test/soap?wsdl';    % test ONLY

createClassFromWsdl(webservice_url);
obj = runcard_wsdl;
methods(obj)
 
targetOpCode    = 'Q410';               % 'fix' ?Optical Test Evaluation? operation code for test step

warehousebin    = 'SCRAP';              % 'fix' Bin assignment required in RunCard
warehouseloc    = 'Scrap Bin';          % 'fix' Location defined in RunCard to place scrapped chips

username        = 'jsanchez';           % 'fix''mornelas'
transType       = 'Scrap';              % 'fix'
comment         = 'FSR Failures';       % 'fix'

%failureChipSerials = cell(0);

% failureChipSerials = {...
%        'P153059.24.013.003'; ...
% 'P153059.24.014.003'; ...
% 'P153059.24.015.007'};
 
%check the status of each serial number
for n=1:size(failureChipSerials,1)
    
    serial = failureChipSerials{n};
    
    [response, error, msg] = getUnitStatus(obj,serial);
    if error ~= 0
        disp(['RunCard exception encountered : ' msg]);
        return
    end
    if ~strcmp(response.opcode, targetOpCode)
        disp(['Invalid opcode ' response.opcode ', expecting ' targetOpCode]);
        continue
    end
    if ~strcmp(response.status, 'IN QUEUE') && ~strcmp(response.status, 'IN PROGRESS')
        disp(['Invalid status ' response.status ' for serial ' response.serial]);
        continue
    end
 
    disp([response.workorder ': ' response.serial ' is ' response.status]);

    transactionInfo.username = username;
    transactionInfo.transaction = transType;
    transactionInfo.serial = response.serial;
    transactionInfo.workorder = response.workorder;
    transactionInfo.seqnum = response.seqnum;
    transactionInfo.opcode = response.opcode;
    
    transactionInfo.warehousebin = warehousebin;
    transactionInfo.warehouseloc = warehouseloc;
    transactionInfo.comment = comment;
    
    transactionData = '';
    transactionBOM = '';
 
    [error, msg] = transactUnit(obj,transactionInfo, transactionData, transactionBOM);
    if error ~= 0
        disp(['RunCard exception encountered when doing a ' transactionInfo.transaction ' transaction: : ' msg]);
        return
    else
        disp(msg);
    end 
end % for each failure chip serial