function [  input_params_struct, error_message  ] = ...
    pwo_get_excel_input_params( an_excel_file_name,excel_tab_number)
%pwo_get_excel_input_params  returns a cell with the set input params:
% Input:  an_excel_file_name  Excel file name (full path) with data to be processed with the pick_to_wo app.
%         excel_tab_number    Excel tab number(BE AWARE THAT EVEN HIDEN TABS ARE PART OF EXCEL NUMBERING:
%                             Warning: Do not hide tabs in excel: It is very misleading.
%
% Output: a cell with 6 entries: in two columns:
% Assumptions:
%   1 - column one is the parameter name
%   2 - column 2 is the parameter value
%   3 - column 3 is the description: Ignored.
%   4 - the indices are in ascending order. and there are no dupplicated indices.error_message  
% sample_call:
% [  input_params_struct, error_message  ] = ...
%     pwo_get_excel_input_params( 'C:\Users\jsanchez\Documents\pick_to_wo\configure\die_scap_configure.xlm',1)
error_message           = '';
input_params_struct     = struct;
input_params_cnt        = 6; % jas_jardcoded: number of expected input parameters in the excel file. Beyond that it will be ignored.
% read the whole sheet as a table:
sheet_table= readtable(an_excel_file_name...
    ,'ReadVariableNames',false,'FileType','spreadsheet'...
    ,'Sheet',excel_tab_number  );
cur_row             = 0;
row_found_cnt       = size(sheet_table,1); %
col_found_cnt       = size(sheet_table,2); %
if ( (col_found_cnt <2) ||  (row_found_cnt <input_params_cnt) )
    error_message = 'ERROR: Invalid Excel file format. Reading input parameters. Fix it and try again. \nExpect at least 2 cols and 11 rows!';
    return;
end

try
    input_params_names  = table2cell(sheet_table(2:input_params_cnt+1,1)); % first col is the  pwo_names param
    input_params_names  = strrep(input_params_names,'''','');
    input_params_values = table2cell(sheet_table(2:input_params_cnt+1,2)); % second col is the pwo_values param
    
%     cfg_params_wanted_cel = { 'webservice_url','wo_number','part_number'...
%         ,'warehouse_bin','warehouse_loc','user_name' ...
%          };
    
        cfg_params_wanted_struct = struct('webservice_url',''... 
            ,'wo_number',''... 
            ,'part_number',''...
            ,'warehouse_bin',''...
            ,'warehouse_loc',''...
            ,'user_name',''...       
            );
    
    found_fields = isfield(cfg_params_wanted_struct, input_params_names);
    if (sum(found_fields) == input_params_cnt)
        % FOUND ALL THE NEEDED PARAMETERS: Assume they are in order.
        cur_row = 1;
        input_params_struct.webservice_url   = input_params_values{1};
        cur_row = 2;
        input_params_struct.wo_number     	= input_params_values{2};
        cur_row = 3;
        input_params_struct.part_number 	= input_params_values{3};
        cur_row = 4;
        input_params_struct.warehouse_bin 	= input_params_values{4};
        cur_row = 5;
        input_params_struct.warehouse_loc  	= input_params_values{5};        
        cur_row = 6;
        input_params_struct.user_name   	= input_params_values{6};
                                            
        % OK TAB_1: configure. Now read tab_2: data
        input_params_struct.die_pick_to_wo_data = {};
        [ die_pick_to_wo_data, error_message ] = pwo_get_excel_range( an_excel_file_name ,2);
        if (~(isempty(error_message)))
            fprintf('\n Error: DIE PICK TO WO read count   = %-3d ',size(die_pick_to_wo_data,1));
            fprintf('\n Error: Partial: Reading DIE PICK TO WO    ... ERROR           ');
            return;
        else
            % OK READING DATA: Save it
            input_params_struct.die_pick_to_wo_data = die_pick_to_wo_data;
            fprintf('\n DIE PICK TO WO read count   = %-3d ',size(die_pick_to_wo_data,1));
            fprintf('\n Reading DIE PICK TO WO    ... DONE           ');            
        end
    else
        error_message = 'ERROR: Invalid Excel file format. Fix it and try again:\n';
        fprintf('\n%s',error_message);        
        fprintf('\n webservice_url	= %-s',input_params_values{1});
        fprintf('\n wo_number    	= %-s',input_params_values{2});
        fprintf('\n part_number	    = %-s',input_params_values{3});
        fprintf('\n warehouse_bin 	= %-s',input_params_values{4});
        fprintf('\n warehouse_loc 	= %-s',input_params_values{5});        
        fprintf('\n user_name    	= %-s',input_params_values{6});
        return;
    end
catch
    error_message = sprintf('ERROR: Invalid Input Parameters Excel file format. \nFix it and try again. Error Reading row: %4d',cur_row);
    return;
end % catch
fprintf('\n Configuration Parameters:    \n');
fprintf('\n webservice_url  = %-s',input_params_values{1});
fprintf('\n wo_number     	 = %-s',input_params_values{2});
fprintf('\n part_number      = %-s',input_params_values{3});
fprintf('\n warehouse_bin 	 = %-s',input_params_values{4});
fprintf('\n warehouse_loc 	 = %-s',input_params_values{5});
fprintf('\n user_name       = %-s',input_params_values{6});

end % fn pwo_get_excel_input_params
     




