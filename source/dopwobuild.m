% script: dodsbuild
% ref:    building pick_to_wo executable: hardcoded: run_card production or run_card_test   alex vs mario part_number
close all;
clear;
clear obj;
clear all;


cache_dir                  = 'C:\Users\jsanchez\Documents\Temp\jsanchez\mcrCache9.1\';

% Check if the cfg folder exists:
if ( ( exist(cache_dir,'dir') == 7))
    % Folder Exist and it is a directory:  If dir exists: Remove all cache dirs
    cd  (cache_dir);
    % CMD for loop to recursivelly remove all dirs: To start clean and avoid compiling with the wrong runcard db.
    !for /d %G in ("dies*") do rd /s /q "%~G"
end

% WE ARE CLEAN: Ready to compile: 

cd  ('C:\Users\jsanchez\Documents\pick_to_wo\source\');
utils_save_all_editor_open_files;

% to compile:
mcc     -m  pick_to_wo     -o  pick_to_wo
%   mcc -m pick_to_wo  -a ..\configure  -o  pick_to_wo -R 'logfile','pick_to_wo.log'
% -a @runcard_wsdl
!copy pick_to_wo.exe C:\Users\jsanchez\Documents\pick_to_wo\exe\
!move pick_to_wo.exe \\genstore2\Users\Storage\jsanchez\Documents\pick_to_wo\exe\
% !C:\Users\jsanchez\Documents\pick_to_wo\exe\pick_to_wo.exe

% ml_doc: https://www.mathworks.com/products/compiler/supported/compiler_support.html
% h = matlab.desktop.editor.getAll; h.close
% cd 'C:\Users\jsanchez\Documents\algo2\source'
% cd 'C:\Users\jsanchez\Documents\fcc\source'
% cd 'C:\Users\jsanchez\Documents\pick_to_wo\source'

%   ~/Documents/Temp/jsanchez/mcrCache9.1/dies1/pick_to_wo/@runcard_wsdl

% Original character   Escaped character
% "                    &quot;
% '                    &apos;
% <                    &lt;
% >                    &gt;
% &                    &amp;



