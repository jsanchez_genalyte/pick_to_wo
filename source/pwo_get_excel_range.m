function [ pick_wo_data, error_message  ] = ...
    pwo_get_excel_range( an_excel_file_name,excel_tab_number)
%DS_GET_EXCEL_RANGE returns a cell with 1 set of range of data: one for 1 block of data in the given sheet_table.
% Input:  an_excel_file_name  Excel file name (full path) with data to be processed with the std_curve_fit app.
%         excel_tab_number    Excel tab number(BE AWARE THAT EVEN HIDEN TABS ARE PART OF EXCEL NUMBERING:
%                             Warning: Do not hide tabs in excel: It is very misleading.
% Output: a cell with one  entries: one for the list of die to pick into the wo found.
% Assumptions:
%   1 - One colum. First row is a dummy title
%   2 - the second row specifies the length of the die to pick into the wo
%   3 - the die to pick into the wo indices are in ascending order. and there are no dupplicated indices.
% pipeline:  die_pick_to_wo  -->  pwo_part_bin_update --> pwo_read_pwo_master_file --> pwo_get_excel_range
error_message          = '';
pick_wo_data           = {};  % (die_pick_to_wo_max_cnt,1);

% read the whole sheet as a table:
sheet_table= readtable(an_excel_file_name...
    ,'ReadVariableNames',false,'FileType','spreadsheet'...
    ,'Sheet',excel_tab_number  );
pwo_len_cel   = table2cell(sheet_table(2,1)); % second row value is the pwo_data_length (only one col)
try
    pwo_len_value        = str2double(pwo_len_cel{1});
    pick_wo_data     = table2cell(sheet_table(3:pwo_len_value+2,1)); % 3rd to end is die pick to wo indices.
catch
    error_message = sprintf('ERROR: Invalid Excel file format.  Tab 2: Need only 1 column: \n row_1: title  \n row_2: cnt \n row 3 to N: data. Fix it and try again');
    return;
end % catch:
end % fn pwo_get_excel_range

